/**
 * English language
 * @type {Object}
 *
 * Example: {{ $t('someWord') }}
 */

const en = {
  helloWorld: 'hello world!'
}

export default en
