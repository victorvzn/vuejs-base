import request from '@/api/server/request'

const cdn = process.env.VUE_APP_END_POINT

export default {
  fetchListData () {
    const url = `${cdn}/todos`
    return request({ method: 'GET', url })
  }

  // postData (form) {
  //   const url = `${cdn}/todos`
  //   const data = qs.stringify(form)
  //   const headers = { 'content-type': 'application/x-www-form-urlencoded' }
  //   return request({ method: 'POST', url, data, headers })
  // }
}
