import axios from 'axios'
import config from './config'

const axiosInstance = axios.create({ baseURL: config.baseURL })

const request = function (payload = { method: 'GET', url: '', data: {} }) {
  return axiosInstance(payload)
    .then(response => response)
    .catch(error => error)
}

export default request
