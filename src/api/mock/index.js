import todos from './data/todos'
import config from './config'

const fetch = (mockData, time = 0) => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(mockData), time)
  })
}

export default {
  fetchListData () {
    console.log('Getting todos data with mock data')
    return fetch(todos, config.defaultMilliseconds)
  }

  // postData (form) {
  //   console.log('Sending todo form with mock data')
  //   return fetch(todos, config.defaultMilliseconds)
  // }
}
