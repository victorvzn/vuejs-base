/* eslint-disable */

export default {
  status: 200,
  data: [
    {
      "id": 1,
      "title": "delectus aut autem T1",
      "completed": false
    },
    {
      "id": 2,
      "title": "quis ut nam facilis et officia qui T2",
      "completed": true
    },
    {
      "id": 3,
      "title": "fugiat veniam minus T3",
      "completed": false
    }
  ]
}
