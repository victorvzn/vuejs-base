import Vue from 'vue'
import Vuex from 'vuex'

import todos from './todos'

Vue.use(Vuex)

const strict = false

const store = new Vuex.Store({
  strict,
  state: {},
  modules: {
    todos
  }
})

export default store
