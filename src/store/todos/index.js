import client from 'api-client'

const todos = {
  namespaced: true,

  state: {
    todos: null
  },

  getters: {
    getData (state) {
      if (state.todos) {
        return state.todos
      }
      return null
    }
  },

  mutations: {
    setData (state, payload) {
      state.todos = payload
    }
  },

  actions: {
    fetchData (context, payload) {
      return new Promise((resolve, reject) => {
        client.fetchListData()
          .then(response => {
            if (response && response.status === 200) {
              if (response.data) {
                context.commit('setData', response.data)
                resolve(response.data)
              } else {
                console.log('An unexpected error ocurred - 01')
              }
            } else {
              console.log('An unexpected error ocurred - 02')
            }
          })
          .catch(error => {
            console.log('An unexpected error ocurred - 03')
            reject(error)
          })
      })
    }
  }
}

export default todos
