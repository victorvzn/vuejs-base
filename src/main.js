import Vue from 'vue'
import App from './App.vue'

import CapitalizeFilter from '@/filters/capitalize'

import router from './router'
import store from './store'
import i18n from './i18n'

import './font-awesome'

Vue.use(CapitalizeFilter)

Vue.config.productionTip = false

i18n.locale = 'en'

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
