const capitalize = {}

function formatText (value) {
  if (value === '') { return '' }
  return value.charAt(0).toUpperCase() + value.slice(1)
}

capitalize.install = function (Vue) {
  Vue.filter('capitalize', (value) => {
    return formatText(value)
  })
}

export default capitalize
