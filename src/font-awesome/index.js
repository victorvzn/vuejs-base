import Vue from 'vue'

// Examples:
// <font-awesome-icon :icon="['fas', 'compass']" />
// <font-awesome-icon :icon="['far', 'compass']" />

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'

import {
  faCompass as fasCompass
} from '@fortawesome/free-solid-svg-icons'

import {
  faCompass as farCompass
} from '@fortawesome/free-regular-svg-icons'

library.add(
  fasCompass,
  farCompass
)

Vue.component('font-awesome-icon', FontAwesomeIcon)
